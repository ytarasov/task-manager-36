package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.service.IProjectService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private static final String NEW_PROJECT_NAME = "new project";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "new project";

    @NotNull
    private static final String UPDATE_PROJECT_NAME = "update project";

    @NotNull
    private static final String UPDATE_PROJECT_DESCRIPTION = "update project";

    @NotNull
    private static final String CHANGE_STATUS_NAME = "test change status";

    @NotNull
    private static final String REMOVE_PROJECT_NAME = "remove";

    @Before
    public void setup() {
        projectRepository.add(new Project("test project 1"));
        projectRepository.add(new Project("test project 2"));
        projectRepository.add(new Project("test project 3"));
    }

    @After
    public void tearDown() {
        projectRepository.clear();
    }

    @Test
    public void findAll() {
        final int expectedSize = projectService.getSize();
        @NotNull final List<Project> projects = projectService.findAll();
        final int currentSize = projects.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithComparator() {
        final int expectedSize = projectService.getSize();
        @Nullable final List<Project> projects = projectService.findAll(Sort.BY_STATUS.getComparator());
        Assert.assertNotNull(projects);
        final int currentSize = projects.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findAllWithSort() {
        final int expectedSize = projectService.getSize();
        @Nullable final List<Project> projects = projectService.findAll(Sort.BY_STATUS);
        Assert.assertNotNull(projects);
        final int currentSize = projects.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(""));
        @NotNull final Project project = new Project(NEW_PROJECT_NAME);
        projectService.add(project);
        @Nullable final Project project1 = projectService.findOneById(project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void findOneByIndex() throws AbstractException {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(500));
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final Project project = new Project();
        int i = 0;
        for (Project project1 : projects) {
            if (i == 2) {
                project.setId(project1.getId());
                project.setName(project1.getName());
                break;
            }
            i++;
        }
        @Nullable final Project project1 = projectService.findOneByIndex(i);
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void getSize() {
        @Nullable final List<Project> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        final int expectedSize = projects.size();
        final int currentSize = projectService.getSize();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void changeStatusById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userId, "", Status.IN_PROGRESS));
        @NotNull final Project project = new Project(CHANGE_STATUS_NAME);
        project.setUserId(userId);
        projectService.add(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, project.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById("", project.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeProjectStatusById(userId, project.getId(), null));
        @Nullable final Project project1 = projectService.changeProjectStatusById(userId, project.getId(), Status.COMPLETED);
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
        Assert.assertEquals(project1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void changeStatusByIndex() throws AbstractException {
        @NotNull final Project project = new Project(CHANGE_STATUS_NAME);
        project.setUserId(userId);
        projectService.add(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex("", 3, Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(null, 3, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(userId, -5, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(userId, 50, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(userId, null, Status.COMPLETED));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeProjectStatusByIndex(userId, 3, null));
        @NotNull final Project project1 = projectService.changeProjectStatusByIndex(userId, 3, Status.COMPLETED);
        Assert.assertEquals(project.getId(), project1.getId());
        Assert.assertEquals(project1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById("", project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userId, null, UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userId, "", UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userId, project.getId(), null, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userId, project.getId(), "", UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, ""));
        @NotNull final Project project1 = projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void updateByIndex() throws AbstractException {
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        @NotNull final String nameNew = "test1";
        @NotNull final String descriptionNew = "description new";
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(null, 0, UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex("", 0, UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(userId, -5, UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(userId, 500, UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(userId, 0, null, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(userId, 0, "", UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateByIndex(userId, 0, UPDATE_PROJECT_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateByIndex(userId, 0, UPDATE_PROJECT_NAME, ""));
        @NotNull final Project project1 = projectService.updateByIndex(userId, 0, nameNew, descriptionNew);
        Assert.assertEquals(nameNew, project1.getName());
        Assert.assertEquals(descriptionNew, project1.getDescription());
    }

    @Test
    public void remove() throws AbstractException {
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.remove(null));
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectService.add(project);
        projectService.remove(project);
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void removeById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(""));
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectService.add(project);
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractException {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(-5));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(500));
        @NotNull final Project project = new Project(REMOVE_PROJECT_NAME);
        projectService.add(project);
        projectService.removeByIndex(3);
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void findAllWithUserId() throws AbstractException {
        @Nullable final String nullUserId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(nullUserId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(""));
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME);
        @Nullable final List<Project> projects = projectService.findAll(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllWithUserIdAndComparator() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(null, Sort.BY_CREATED.getComparator()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll("", Sort.BY_CREATED.getComparator()));
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME);
        @Nullable final List<Project> projects = projectService.findAll(userId, Sort.BY_CREATED.getComparator());
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findOneByIdAndUserId() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(userId, ""));
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(null, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById("", project.getId()));
        @Nullable final Project project1 = projectService.findOneById(userId, project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getName(), project1.getName());
    }

    @Test
    public void findOneByIndexAndUserId() throws AbstractException{
        @NotNull final Project project = projectService.create(userId, NEW_PROJECT_NAME);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(null, 3));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex("", 3));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(userId, -5));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(userId, 500));
        @Nullable final Project project1 = projectService.findOneByIndex(userId, 0);
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getName(), project1.getName());
    }

    @Test
    public void set() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(new Project("test set 1"));
        projects.add(new Project("test set 1"));
        projectService.set(projects);
        Assert.assertEquals(projects.size(), projectService.getSize());
    }

    @Test
    public void getSizeWithUserId() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(null));
        projectService.create(userId, NEW_PROJECT_NAME);
        final int sizeExpected = projectService.findAll(userId).size();
        final int sizeCurrent = projectService.getSize(userId);
        Assert.assertEquals(sizeExpected, sizeCurrent);
    }

    @Test
    public void clear() {
        projectService.clear();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void clearWithUserId() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(""));
        projectService.add(userId, new Project("test clear 1"));
        projectService.add(userId, new Project("test clear 2"));
        projectService.add(userId, new Project("test clear 3"));
        projectService.clear(userId);
        Assert.assertEquals(0, projectService.getSize(userId));
        Assert.assertNotEquals(0, projectService.getSize());
    }

}
