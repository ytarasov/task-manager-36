package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IndexIncorrectException;
import ru.t1.ytarasov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.TaskIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;
    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @NotNull
    @Override
    public Project removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable List<Task> tasksToRemove = taskRepository.findAllTasksByProjectId(userId, project.getId());
        if (tasksToRemove == null) throw new TaskNotFoundException();
        for (@NotNull final Task task : tasksToRemove) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
        return project;
    }

    @NotNull
    @Override
    public Project removeProjectByIndex(@Nullable final String userId, int projectIndex) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex < 0 || projectIndex >= projectRepository.getSize(userId)) throw new IndexIncorrectException();
        if (projectIndex >= projectRepository.getSize()) throw new IndexIncorrectException();
        @Nullable final Project project = projectRepository.findOneByIndex(userId, projectIndex);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final List<Task> tasks = taskRepository.findAllTasksByProjectId(userId, project.getId());
        if (tasks == null) {
            projectRepository.removeByIndex(userId, projectIndex);
            throw new TaskNotFoundException();
        }
        for (Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeByIndex(userId, projectIndex);
        return project;
    }

    @Override
    public void clearAllProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<Project> projects = projectRepository.findAll(userId);
        if (projects == null) return;
        @Nullable final List<Task> tasks = taskRepository.findAll(userId);
        if (tasks == null) {
            projectRepository.clear(userId);
            return;
        }
        for (final Project project : projects) {
            for (final Task task : tasks) {
                if (project.getId().equals(task.getProjectId())) taskRepository.removeById(userId, task.getId());
            }
        }
        projectRepository.clear(userId);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
