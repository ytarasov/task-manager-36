package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.IRepository;
import ru.t1.ytarasov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @Override
    public @NotNull List<M> findAll() {
        return models;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable Comparator comparator) {
        if (comparator == null) return findAll();
        final List<M> result = findAll();
        result.sort(comparator);
        return result;
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        this.models.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(final @Nullable String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(final @Nullable String id) {
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(final int index) {
        return models.get(index);
    }

    public void clear() {
        models.clear();
    }

    @Nullable
    @Override
    public M remove(M model) {
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(final int index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

}
