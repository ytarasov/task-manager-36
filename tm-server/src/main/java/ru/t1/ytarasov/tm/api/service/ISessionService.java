package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
