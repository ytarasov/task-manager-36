package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.IUserOwnedRepository;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(@Nullable final String userId) {
        final @Nullable List<M> result = findAll(userId);
        if (result == null) return;
        models.removeAll(result);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || "".equals(userId)) return Collections.emptyList();
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        @Nullable final List<M> result = findAll(userId);
        if (comparator == null) return result;
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .filter(m -> userId.equals(m.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        @Nullable final List<M> userZOwnedModels = findAll(userId);
        if (userZOwnedModels == null) return null;
        return userZOwnedModels.get(index);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public M removeById(@Nullable String userId, @NotNull String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(@Nullable final String userId, int index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @NotNull final M model) {
        if (userId == null || userId.isEmpty()) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public M remove(@NotNull String userId, final @NotNull M model) {
        return removeById(userId, model.getId());
    }

}
