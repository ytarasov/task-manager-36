package ru.t1.ytarasov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Session;
import ru.t1.ytarasov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    String login(@Nullable String login, @Nullable String password) throws AbstractException;

    @NotNull User check(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout(@Nullable final Session session);

    boolean isAuth();

    @NotNull
    String getUserId() throws AbstractException;

    @Nullable
    User getUser() throws AbstractException;

    void checkRoles(@Nullable Role[] roles) throws AbstractException;

    @NotNull
    @SneakyThrows
    Session validateToken(@Nullable String token);
}
