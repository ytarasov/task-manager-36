package ru.t1.ytarasov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataYamlLoadRequest extends AbstractDataRequest {

    public DataYamlLoadRequest(@Nullable String token) {
        super(token);
    }

}
