package ru.t1.ytarasov.tm.dto.request.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable String token) {
        super(token);
    }

}
