package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataJsonFasterXmlSaveRequest;

public final class DataJsonFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-json-fasterxml-save";

    @NotNull
    public static final String DESCRIPTION = "Saves to json by FasterXml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("JSON FASTERXML SAVE");
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonFasterXmlSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
