package ru.t1.ytarasov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.task.TaskShowByProjectIdRequest;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @Nullable
    public static final String DESCRIPTION = "Show task by project id.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[FIND ALL TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final List<Task> tasks = getTaskEndpoint().showTaskByProjectId(request).getTasks();
        if (tasks == null) return;
        for (Task task : tasks) System.out.println(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
