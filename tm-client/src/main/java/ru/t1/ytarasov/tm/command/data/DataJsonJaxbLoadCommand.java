package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataJsonJaxbLoadRequest;

public final class DataJsonJaxbLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-json-jaxb-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from json by JAXB";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[JSON JAXB LOAD]");
        getDomainEndpoint().loadDataJsonJaxb(new DataJsonJaxbLoadRequest(getToken()));
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
