package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataBase64LoadRequest;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    public static final String NAME = "data-base64-load";

    public static final String DESCRIPTION = "Load from base64 file";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
