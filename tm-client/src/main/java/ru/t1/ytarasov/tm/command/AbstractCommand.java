package ru.t1.ytarasov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.model.ICommand;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.IOException;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    @Override
    public abstract void execute() throws AbstractException, IOException, ClassNotFoundException;

    @Nullable
    @Override
    public abstract String getName();

    @Nullable
    @Override
    public abstract String getArgument();

    @Nullable
    @Override
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    protected String getToken() {
        return serviceLocator.getTokenService().getToken();
    }

    protected void setToken(@Nullable String token) {
        serviceLocator.getTokenService().setToken(token);
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ": " + description;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
