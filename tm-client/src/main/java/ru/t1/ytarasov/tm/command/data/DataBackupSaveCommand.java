package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataBackupSaveRequest;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-backup-save";

    @NotNull
    public static final String DESCRIPTION = "Save backup";

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
